<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link https://ja.wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// 注意:
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.osdn.jp/%E7%94%A8%E8%AA%9E%E9%9B%86#.E3.83.86.E3.82.AD.E3.82.B9.E3.83.88.E3.82.A8.E3.83.87.E3.82.A3.E3.82.BF 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define( 'DB_NAME', '01022' );

/** MySQL データベースのユーザー名 */
define( 'DB_USER', 'root' );

/** MySQL データベースのパスワード */
define( 'DB_PASSWORD', 'local_password' );

/** MySQL のホスト名 */
define( 'DB_HOST', 'db' );

/** データベースのテーブルを作成する際のデータベースの文字セット */
define( 'DB_CHARSET', 'utf8mb4' );

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define( 'DB_COLLATE', '' );

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'N%F9[30_(9bpH13aJM,&topW?o]3DNwWcL|C?rge6Bx.rj*GCy>W(TTI@thpFeqV' );
define( 'SECURE_AUTH_KEY',  '_vLr{QS-@>A%d;~tJ/h~RpyScp8:|S):J^g;K 1T%U#IkT4!w[4^mww(NL4&qz!r' );
define( 'LOGGED_IN_KEY',    '1GIf]3}WyIHKi:pbJdDDJ+b>K}(8wVm}@1|fxYcGrh;oZ*]Ue(Tm@W[t4?;Fq`^c' );
define( 'NONCE_KEY',        'C{T$34[k}BV4-{i@uyQZ:nwIn-UN/A*S v$V>^x[Fsa`r+PP&b;b{eyI*(HOq67W' );
define( 'AUTH_SALT',        ']Pun*y6O/p]?hI7#cQ#+J36f-@{p)TU`*1=gSLv)#cZ0W>l&${87m+K4DY1t4I!c' );
define( 'SECURE_AUTH_SALT', 'fl_yD5J4ID68s{Z2=}~>4<nUWRcx?HD6<_Hr|QV%][l8,mTpr{XR};hm4D%`Y~wR' );
define( 'LOGGED_IN_SALT',   'XP-nu&eTB[>:/x/qO_REOWd6*rvt++f=c-!lQ,qK0a[0!}NipU wz}$%b#bUwP1e' );
define( 'NONCE_SALT',       'i7-mp1((+HHE^APAtn.+z;t,;AS;jHgfT0]oVMUjRYxSqx@{m[SIJq]n^G; 2P@b' );

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数についてはドキュメンテーションをご覧ください。
 *
 * @link https://ja.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* カスタム値は、この行と「編集が必要なのはここまでです」の行の間に追加してください。 */



/* 編集が必要なのはここまでです ! WordPress でのパブリッシングをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
