<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link https://ja.wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// 注意:
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.osdn.jp/%E7%94%A8%E8%AA%9E%E9%9B%86#.E3.83.86.E3.82.AD.E3.82.B9.E3.83.88.E3.82.A8.E3.83.87.E3.82.A3.E3.82.BF 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define( 'DB_NAME', '01022' );

/** MySQL データベースのユーザー名 */
define( 'DB_USER', 'root' );

/** MySQL データベースのパスワード */
define( 'DB_PASSWORD', 'local_password' );

/** MySQL のホスト名 */
define( 'DB_HOST', 'db' );

/** データベースのテーブルを作成する際のデータベースの文字セット */
define( 'DB_CHARSET', 'utf8mb4' );

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define( 'DB_COLLATE', '' );

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '*km3N&N{Nt}11j]h$D:)F!u.2aF,J|<u5~#TRk)$f(.3;/s7UU;HETw|UNVXzNkf' );
define( 'SECURE_AUTH_KEY',  'TM!^$N1P*Cu&~)^rO=D<8V<*ep(UpHyaNQ]f:!j#=,my~avK4/AHTMG}AJe:0z?B' );
define( 'LOGGED_IN_KEY',    'BVTj:yA>Zb|7xL^5b6%,&QRU4]h:&$Y W4w9 ioXJFIh1Y|q|b=F]h?8I=~t^NUw' );
define( 'NONCE_KEY',        'F^vbct,Mpfh-hseQ$yoJeo8e6EKv%9)Z{&-1`FLfG-DjDcAYZ,;4K[L:d;/PN+dA' );
define( 'AUTH_SALT',        'Z[VfDH 0dx9A$rbFP>9$5Ivf0vU1W>#r+3WF[*Z|@Y44gTo}zTQKZN*tNd~YO$>{' );
define( 'SECURE_AUTH_SALT', '.V$E;-NNy|b|YgumXK_x}S$JujSXk=n*cZnP!T[hscP${bL8Bw;!s_BQp:F2Pi _' );
define( 'LOGGED_IN_SALT',   'JQ]m#D*n1BWdiCp]yHUe=B:nyV)b5Pp3X&]ytBs#69WL`PK:-$#pRF=hkS& ]<6m' );
define( 'NONCE_SALT',       '@NeO `PQ0Cq8e_Jf%I]zj_wRWuO|]Z{-$zC5da;[aTwmzL0p80%/)-zQw-[g[C~/' );

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数についてはドキュメンテーションをご覧ください。
 *
 * @link https://ja.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* カスタム値は、この行と「編集が必要なのはここまでです」の行の間に追加してください。 */



/* 編集が必要なのはここまでです ! WordPress でのパブリッシングをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
